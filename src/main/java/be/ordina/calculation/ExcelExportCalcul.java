package be.ordina.calculation;

import org.springframework.stereotype.Component;

@Component
public class ExcelExportCalcul {

	public Integer hospitalInsurance(int number){
		return number;
	}
	
	public Integer insurPensiondeceasedInval(int number){
		return number;
	}
	
	public Integer fuel(int number){
		return number;
	}
	
	public Integer totalFte(int number){
		return number;
	}
	
	public Integer mealEcoCheques(int number){
		return number;
	}
	
	public Integer billable(int number){
		return number;
	}
	
	public Integer endDate(int number){
		return number;
	}
	
	public Integer stake(int number){
		return number;
	}
	
	public Integer costTarif(int number){
		return number;
	}
	
	public Integer sellTarif(int number){
		return number;
	}
}
