package be.ordina.main;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import be.ordina.service.ImportService;

public class ForecastApp {

	public static void main(final String[] args) {
		// Create springcontext
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		Properties prop = loadConfigFile(context, "config.properties");

		// files
		String bloxReport = prop.getProperty("input.bloxreport.path") + prop.getProperty("input.bloxreport.filename");
		String fleetReport = prop.getProperty("input.fleetreport.path")
				+ prop.getProperty("input.fleetreport.filename");
		String flotillaReport = prop.getProperty("input.flotillareport.path")
				+ prop.getProperty("input.flotillareport.filename");
		String fleetExtendedReport = prop.getProperty("input.fleetextendedreport.path")
				+ prop.getProperty("input.fleetextendedreport.filename");

		File bloxReportFile = new File(bloxReport);
		File fleetReportFile = new File(fleetReport);
		File flotillaReportFile = new File(flotillaReport);
		File fleetExtendedReportFile = new File(fleetExtendedReport);

		ImportService importService = (ImportService) context.getBean("importServiceImpl");

		// importService.importExcelFile(bloxReportFile, "BloxReport");
		importService.importExcelFile(fleetReportFile, "FleetReport");
		// importService.importExcelFile(flotillaReportFile, "FlotillaReport");
		// importService.importExcelFile(fleetExtendedReportFile,
		// "FleetExtendedReport");
		System.out.println("insert completed");

		/*
		 * System.out.println("save data from db");
		 * 
		 * try { FileInputStream file = new FileInputStream(new File(
		 * "c:\\__Ordina_forecast\\Forecast\\Forecast_03-2014_Dummy_Original.xls"
		 * ));
		 * 
		 * // Get the workbook instance for XLS file HSSFWorkbook workbook = new
		 * HSSFWorkbook(file);
		 * 
		 * // write the right data in the right worksheet at the right
		 * excelExport.WriteUploadDataMw(workbook, mongoDb);
		 * excelExport.WriteInhuurDirInput(workbook, mongoDb); // position.
		 * 
		 * FileOutputStream out = new FileOutputStream(new File(
		 * "c:\\__Ordina_forecast\\Forecast\\Forecast_03-2014_Dummy_Original.xls"
		 * )); excelExport.writeToFile(out, workbook); } catch
		 * (FileNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); }
		 */
		((ClassPathXmlApplicationContext) context).close();
		System.out.println("Done!");
	}

	private static Properties loadConfigFile(final ApplicationContext context, final String propertiesFile) {
		Properties prop = new Properties();
		try {
			prop.load(context.getClassLoader().getResourceAsStream(propertiesFile));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return prop;
	}
}
