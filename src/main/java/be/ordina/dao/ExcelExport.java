package be.ordina.dao;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.mongodb.DB;

public interface ExcelExport {

	void writeToFile(FileOutputStream file, HSSFWorkbook workbook) throws IOException;
	void WriteUploadDataMw(HSSFWorkbook workbook, DB database);
	void WriteInhuurDirInput(HSSFWorkbook workbook, DB database);
}
