package be.ordina.dao.impl;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

@Component("mongo")
public class Mongo {

	private MongoClient mongoClient;
	private DB db;
	private DBCollection collection;
	private String dbName;

	@Autowired
	public Mongo(@Value("forecast") final String dbName) {
		try {
			mongoClient = new MongoClient("localhost", 27017);
			this.db = mongoClient.getDB(dbName);
			this.dbName = dbName;
		} catch (MongoException | UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void drop() {
		mongoClient.dropDatabase(dbName);
	}

	public void listCollections() {
		Set<String> colls = db.getCollectionNames();

		for (String s : colls) {
			System.out.println(s);
		}
	}

	public void listIndex() {
		List<DBObject> list = collection.getIndexInfo();

		for (DBObject o : list) {
			System.out.println("\t" + o);
		}
	}

	public void setCollection(final String col) {
		this.collection = db.getCollection(col);
	}

	public void insert(final BasicDBObject doc) {
		this.collection.insert(doc);
	}

	public DBCollection getCollection() {
		return collection;
	}

	public void createDefaultIndex() {
		collection.createIndex(new BasicDBObject("_id", 1));
	}

	public void printCollectionOnScreen() {
		DBCursor cursor = collection.find();
		while (cursor.hasNext()) {
			System.out.println(cursor.next());
		}
	}
}
