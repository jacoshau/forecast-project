package be.ordina.dao.impl;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import be.ordina.calculation.ExcelExportCalcul;
import be.ordina.dao.ExcelExport;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Repository("excelExportImpl")
public class ExcelExportImpl implements ExcelExport {

	@Autowired
	private ExcelExportCalcul calcul;
		
	public void writeToFile(FileOutputStream out, HSSFWorkbook workbook)
			throws IOException {

		workbook.write(out);
		out.close();
	}

	public void WriteUploadDataMw(HSSFWorkbook workbook,
			DB database) {
		DBCollection table = database.getCollection("BloxReport");
		DBCursor result = table.find();
		
		HSSFSheet sheet = workbook.getSheetAt(0); // Upload data MW is the first worksheet of the excel.
		
		int row = 5; // data starts on 6th row
		while (result.hasNext()) {
			WriteSapNumber(sheet, result.next(), row, 0); //SAP number is first column of this file
			
			/*
			 * TODO Uncomment when import works with proper names 
			WriteDivisie(sheet, result.next(), row, 1);
			WriteBu(sheet, result.next(), row, 2);
			WriteDivBu(sheet, result.next(), row, 3);
			WriteEmployeeName(sheet, result.next(), row, 4);
			WriteEmployeeFirstName(sheet, result.next(), row, 5);
			WriteStartDate(sheet, result.next(), row, 7);
			WriteEndDate(sheet,result.next(),row,8);
			WriteDir(sheet, result.next(), row, 9);
			WriteInd(sheet, result.next(), row, 10);
			WriteTotalFte(sheet, result.next(), row, 11); //calculation
			WriteBrutoSalaris(sheet, result.next(), row, 12);
			WriteForfaitRepr(sheet, result.next(), row, 17);
			WriteForfaitOnkost(sheet, result.next(), row, 18);
			WriteGsmVergoed(sheet, result.next(), row, 19);
			WriteGsmInvoice(sheet, result.next(), row, 20);
			
			DBObject carDocument = findDocumentById(result.next().get("_id").toString(),database,"FleetReport");//neemt de _id van BloxReport en zoekt met deze id een document op _id van Fleetreport 
			WriteAutoBudgetNoFuel(sheet, carDocument, row, 23); 
			WriteCo2(sheet, carDocument, row, 24); 
			
			WriteSocAbon(sheet, result.next(), row, 25);*/
			row++;
		}
		
	}
	
	public void WriteInhuurDirInput(HSSFWorkbook workbook,
			DB database) {
		DBCollection table = database.getCollection("BloxReport");
		DBCursor result = table.find();
		
		HSSFSheet sheet = workbook.getSheetAt(4); // InhuurDirInput is the 5th worksheet of the excel.
		
		int row = 5; // data starts on 6th row
		while (result.hasNext()) {
			WriteSapNumber(sheet, result.next(), row, 0); //SAP number is first column of this file
			
			/*
			 * TODO Uncomment when import works with proper names 
			WriteBu(sheet, result.next(), row, 3);
			WriteEmployeeName(sheet, result.next(), row, 1);
			WriteEmployeeFirstName(sheet, result.next(), row, 2);
			WriteStartDate(sheet, result.next(), row, 5);
			*/
			row++;
		}
	}
	
	/*Available in BloxReport table*/
	private void WriteSapNumber(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("_id").toString()); //TODO change to proper db name (SapNumber)
	}
	
	/*Available in BloxReport table*/
	private void WriteDivisie(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Department").toString().split("-")[0]); 
	}
	
	/*Available in BloxReport table*/
	private void WriteBu(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Department").toString().split("-")[1]); 
	}
	
	/*Available in BloxReport table*/
	private void WriteDivBu(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Department").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteEmployeeName(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("LastName").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteEmployeeFirstName(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("FirstName").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteStartDate(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("EmploymentDate").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteEndDate(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("OutOfServiceDate").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteDir(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("FreeZone1").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteInd(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("FreeZone2").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteBrutoSalaris(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("GrossSalary").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteForfaitRepr(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Factor3").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteForfaitOnkost(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Factor1").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteGsmVergoed(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Factor6").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteGsmInvoice(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("Factor7").toString()); 
	}
	
	/*Available in FleetReport table*/
	private void WriteAutoBudgetNoFuel(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("MonthPrice").toString()); 
	}
	
	/*Available in FleetReport table*/
	private void WriteCo2(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("CO2Contribution").toString()); 
	}
	
	/*Available in BloxReport table*/
	private void WriteSocAbon(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(object.get("GSM3107").toString()); 
	}
	
	/*Calculation, data needed from ???? table*/
	private void WriteTotalFte(HSSFSheet sheet , DBObject object,  int row , int column){
		sheet.getRow(row).getCell(column).setCellValue(calcul.totalFte(/*TODO hier juiste data zetten van object*/ 12));  
	}
	
	private DBObject findDocumentById(String id, DB database , String tableName) {
	    BasicDBObject query = new BasicDBObject();
	    DBCollection collection = database.getCollection(tableName);
	    query.put("_id", new ObjectId(id));
	    DBObject dbObj = collection.findOne(query);
	    return dbObj;
	}
	
}
