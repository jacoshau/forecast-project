package be.ordina.dao.impl;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import be.ordina.dao.ExcelImport;

import com.mongodb.BasicDBObject;

@Repository("excelImportImpl")
public class ExcelImportImpl implements ExcelImport {

	@Autowired
	private Mongo mongo;

	@Override
	public void importExcelFileIntoMongoDBCollection(final File file, final String collectionName) {
		System.out.println("load xml file to db");

		mongo.setCollection(collectionName);

		try {
			mongo.createDefaultIndex();
			Workbook workbook = WorkbookFactory.create(file);

			Sheet sheet = workbook.getSheetAt(0); // first sheet

			insertSheetIntoCollection(sheet);
		} catch (IOException | InvalidFormatException ex) {
			ex.printStackTrace();
		}

		mongo.printCollectionOnScreen();
	}

	private void insertSheetIntoCollection(final Sheet sheet) {
		int rows = sheet.getPhysicalNumberOfRows(); // lookout for empty rows!!
		// Row titleRow = sheet.getRow(0);
		// int numberOfActualCells = titleRow.getPhysicalNumberOfCells();

		// iterate over the rows of a sheet
		for (int rowNum = 1; rowNum < rows; rowNum++) {
			Row row = sheet.getRow(rowNum);
			if (!isRowEmpty(row)) {
				insertRowIntoCollection(row);
			}
		}
	}

	private boolean isRowEmpty(final Row row) {
		for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
				return false;
		}
		return true;
	}

	private void insertRowIntoCollection(final Row row) {
		int numberOfCells = getNumberOfCellsInARow(mongo.getCollection().getName());
		BasicDBObject rowDBObject = new BasicDBObject();

		for (int c = 0; c < numberOfCells; c++) {
			Cell cell = row.getCell(c);
			String columnName = getPredefinedColumnNameAtIndex(mongo.getCollection().getName(), c);
			System.out.print(columnName + " ");
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK) {
				appendCellContentToRowObject(rowDBObject, columnName, cell);
			} else if (columnName.equals("createdDate")) {
				Date date = new Date();
				rowDBObject.append("CreatedDate", date);
			}
		}
		mongo.insert(rowDBObject);
		System.out.println("");
	}

	private BasicDBObject appendCellContentToRowObject(final BasicDBObject rowDBObject, final String columnName,
			final Cell cell) {

		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN:
			rowDBObject.append(columnName, cell.getBooleanCellValue());
			System.out.print(columnName + " : " + cell.getBooleanCellValue() + "\t\t");
			break;
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				rowDBObject.append(columnName, cell.getDateCellValue());
				System.out.print(columnName + " : " + cell.getDateCellValue() + "\t\t");
			} else {
				rowDBObject.append(columnName, cell.getNumericCellValue());
				System.out.print(columnName + " : " + cell.getNumericCellValue() + "\t\t");
			}
			break;
		case Cell.CELL_TYPE_STRING:
			rowDBObject.append(columnName, cell.getStringCellValue());
			System.out.print(columnName + " : " + cell.getStringCellValue() + "\t\t");
			break;
		}
		return rowDBObject;
	}

	private String getPredefinedColumnNameAtIndex(final String collectionName, final int index) {
		String returnValue = "";
		switch (collectionName) {
		case "BloxReport":
			returnValue = bloxReportColumnNameAtIndex[index];
			break;
		case "FleetReport":
			returnValue = fleetReportColumnNameAtIndex[index];
			break;
		case "FlotillaReport":
			returnValue = flotillaReportColumnNameAtIndex[index];
			break;
		case "FleetExtendedReport":
			returnValue = fleetExtendedReportColumnNameAtIndex[index];
			break;
		}
		return returnValue;
	}

	private int getNumberOfCellsInARow(final String collectionName) {
		int returnValue = 0;
		switch (collectionName) {
		case "BloxReport":
			returnValue = bloxReportColumnNameAtIndex.length;
			break;
		case "FleetReport":
			returnValue = fleetReportColumnNameAtIndex.length;
			break;
		case "FlotillaReport":
			returnValue = flotillaReportColumnNameAtIndex.length;
			break;
		case "FleetExtendedReport":
			returnValue = fleetExtendedReportColumnNameAtIndex.length;
			break;
		}
		return returnValue;
	}
}
