package be.ordina.dao;

import java.io.File;

public interface ExcelImport {

	static final String[] bloxReportColumnNameAtIndex = new String[] { "SapNumber", "EmployeeNumber", "FirstName",
			"LastName", "Language", "EmploymentType", "Department", "Gender", "BirthDate", "EmploymentDate",
			"SeniorityDate", "GrossSalary", "GSM3107", "Factor1", "Factor2", "Factor3", "Factor4", "Factor5",
			"Factor6", "Factor7", "OutOfServiceDate", "HoursPerWeek", "FreeZone1", "FreeZone2", "FreeZone3",
			"FreeZone4", "CreatedDate", "_id" };

	static final String[] fleetReportColumnNameAtIndex = new String[] { "Active", "Fullname", "Level", "SapNumber",
			"MonthPrice", "DriverUpgrade", "BenefitsInKindYearly", "BenefitsInKind", "Emission", "CO2Contribution",
			"FiscalDeduct", "FiscalRetailPrice", "LicensePlate", "MonthDate", "CreatedDate", "_id" };

	static final String[] flotillaReportColumnNameAtIndex = new String[] { "LicensePlate", "Make", "Model", "Driver",
			"BU", "CostCenter", "VehicleType", "InternalCategory", "Level", "Company", "ServiceType", "InvoiceDate",
			"InvoiceNumber", "Supplier", "LineDate", "LineDateTo", "Description", "Amount", "Mileage", "Accounting",
			"CreatedDate", "_id" };

	static final String[] fleetExtendedReportColumnNameAtIndex = new String[] { "Active1", "FullName", "LastName",
			"FirstName", "Level", "SapNumber", "StartedDate", "StoppedDate", "Active2", "ContractType", "StartDate",
			"EndDate", "ContractNumber", "MonthPrice", "Supplier", "SubCompany", "DriverUpgrade", "DeliveryDate",
			"BenefitsInKindYearly", "BenefitsInKind", "Emission", "CO2Contribution", "FiscalDeduct",
			"FiscalRetailPrice", "LicensePlate", "Make", "Model", "RegistrationDate", "Status", "StatusDate", "Type",
			"ChassisNumber", "VehicleType", "Period", "CreatedDate", "_id" };

	void importExcelFileIntoMongoDBCollection(File file, String collectionName);
}
