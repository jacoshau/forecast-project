package be.ordina.service;

import java.io.File;

public interface ImportService {
	public void importExcelFile(final File file, final String collectionName);
}
