package be.ordina.service.impl;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import be.ordina.dao.ExcelImport;
import be.ordina.service.ImportService;

@Service("importServiceImpl")
public class ImportServiceImpl implements ImportService {

	@Autowired
	private ExcelImport excelImport;

	public void importExcelFile(final File file, final String collectionName) {
		excelImport.importExcelFileIntoMongoDBCollection(file, collectionName);
	}
}
